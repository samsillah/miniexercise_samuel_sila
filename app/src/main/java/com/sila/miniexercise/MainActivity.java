package com.sila.miniexercise;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.sila.miniexercise.R.layout.activity_main);
    }

    public void launchScreen1(View view) {
        startActivity(new Intent(this, ScreenOne.class));
    }

    public void launchScreen2(View view) {
        startActivity(new Intent(this, ScreenTwo.class));
    }

    public void launchScreen3(View view) {
        startActivity(new Intent(this, ScreenThree.class));
    }
}
